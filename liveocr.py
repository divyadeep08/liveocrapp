
# coding: utf-8

# In[ ]:


from flask_api import FlaskAPI
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask import request
from flask import render_template
from flask import jsonify
from sqlalchemy import text
import re
import numpy as np
import os
import json
from PIL import Image
import base64
from io import BytesIO


# initialize sql-alchemy
db = SQLAlchemy()


app = FlaskAPI(__name__, instance_relative_config=True)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


# In[ ]:

@app.route('/')
def show_all():
    return render_template('info.html')

@app.route('/contact360',methods=['POST'])
def predict():
	import smtplib
	from email.mime.text import MIMEText

	# Define to/from
	sender = 'support@360digitaltransformation.com'
	recipient = 'contact@360digitaltransformation.com'
	data = request.form
	print(data)
	query = str(data.get('your-message'))
	name = str(data.get('your-name'))
	email = str(data.get('your-email'))
	phone = str(data.get('your-phone'))
	website = str(data.get('your-website'))
	# findall() has been used  
    # with valid conditions for urls in string 
	url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', query)
	href = re.findall('href?',query)  
	#query = "360 Course Fee"
	#name ="TEST"
	#email = "Test@gmail.com"
	#phone = "9654901300"
	print(url)
	print(href)
	if url or href:
		return 'fake'
	message = "Name:  "+name + " \nEmail:  "+ email+ " \nPhone: " + phone+ " \nMessage:  " + query+ " \nWebsite:  " + website
	# Create message
	msg = MIMEText(message)
	msg['Subject'] = "Enquiry"
	msg['From'] = sender
	msg['To'] = recipient

	# Create server object with SSL option
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

	# Perform operations via server
	server.login('support@360digitaltransformation.com' , 'support@360dt')
	server.sendmail(sender, [recipient], msg.as_string())
	server.quit()
	return '<html><head><script>window.location="http://www.360digitaltransformation.com/contact.html"</script></head></html>'
 
@app.route('/contact',methods=['POST'])
def contactus():
	import smtplib
	from email.mime.text import MIMEText

	# Define to/from
	sender = 'support@360digitaltransformation.com'
	recipient = 'contact@360digitaltransformation.com'
	data = request.form
	print(data)
	query = str(data.get('your-message'))
	name = str(data.get('your-name'))
	email = str(data.get('your-email'))
	phone = str(data.get('your-phone'))
	website = str(data.get('your-website'))
	redirectit = '<html><head><script>window.location="http://www.360digitaltransformation.com/{0}"</script></head></html>'.format(website)
	for ch in name:
		if ch.isdigit():
			print('chutia1')
			return redirectit
	# findall() has been used  
    # with valid conditions for urls in string 
	url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', query)
	href = re.findall('href?',query)  
	#query = "360 Course Fee"
	#name ="TEST"
	#email = "Test@gmail.com"
	#phone = "9654901300"
	if website == 'index.html':
		website=''
	print(url)
	print(href)
	if url or href:
		return 'fake'
	message = "Name:  "+name + " \nEmail:  "+ email+ " \nPhone: " + phone+ " \nMessage:  " + query
	# Create message
	msg = MIMEText(message)
	msg['Subject'] = "Enquiry"
	msg['From'] = sender
	msg['To'] = recipient

	# Create server object with SSL option
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

	# Perform operations via server
	server.login('support@360digitaltransformation.com' , 'support@360dt')
	server.sendmail(sender, [recipient], msg.as_string())
	server.quit()
	return  redirectit

@app.route('/contactus',methods=['POST'])
def contactuslanding():
	import smtplib
	from email.mime.text import MIMEText

	
	sender = 'support@360digitaltransformation.com'
	recipient = 'contact@360digitaltransformation.com'
	data = request.form
	print(data)
	name = str(data.get('name'))
	email = str(data.get('email'))
	phone = str(data.get('phone'))
	
	redirectit = '<html><head><script>window.location="https://www.360digitaltransformation.com/landing_page/thankyou.html"</script></head></html>'

	for ch in name:
		if ch.isdigit():
			print('chutia1')
			return redirectit
	# findall() has been used  
    # with valid conditions for urls in string 
	#url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', query)
	#href = re.findall('href?',query)  
	#query = "360 Course Fee"
	#name ="TEST"
	#email = "Test@gmail.com"
	#phone = "9654901300"
	
	#print(url)
	#print(href)
	
	message = "Name:  "+name + " \nEmail:  "+ email+ " \nPhone: " + phone
	# Create message
	msg = MIMEText(message)
	msg['Subject'] = "Enquiry"
	msg['From'] = sender
	msg['To'] = recipient

	# Create server object with SSL option
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

	# Perform operations via server
	server.login('support@360digitaltransformation.com' , 'support@360dt')
	server.sendmail(sender, [recipient], msg.as_string())
	server.quit()
	return  redirectit


@app.route('/bvpfeedback',methods=['POST'])
def bvpfeedback():
	import smtplib
	from email.mime.text import MIMEText

	# Define to/from
	sender = 'support@360digitaltransformation.com'
	recipient = 'contact@360digitaltransformation.com'
	data = request.form
	print(data)
	query = str(data.get('your-message'))
	name = str(data.get('your-name'))
	email = str(data.get('your-email'))
	phone = str(data.get('your-phone'))
	rating = str(data.get('your-rating'))
	redirectit = '<html><head><script>window.location="http://www.360digitaltransformation.com/"</script></head></html>'
	for ch in name:
		if ch.isdigit():
			print('chutia')
			return redirectit
	# website = str(data.get('your-website'))
	# findall() has been used  
    # with valid conditions for urls in string 
	url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', query)
	href = re.findall('href?',query)  
	#query = "360 Course Fee"
	#name ="TEST"
	#email = "Test@gmail.com"
	#phone = "9654901300"
	print(url)
	print(href)
	if url or href:
		return ''
	message = "Name:  "+name + " \nEmail:  "+ email+ " \nPhone: " + phone+ " \nMessage:  " + query+ " \nRating:  " + rating
	# Create message
	msg = MIMEText(message)
	msg['Subject'] = "Enquiry"
	msg['From'] = sender
	msg['To'] = recipient

	# Create server object with SSL option
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

	# Perform operations via server
	server.login('support@360digitaltransformation.com' , 'support@360dt')
	server.sendmail(sender, [recipient], msg.as_string())
	server.quit()
	return  redirectit
if __name__ == '__main__':
   app.debug = True
   port = int(os.environ.get("PORT", 5000))
   app.run(host='0.0.0.0', port=port)
